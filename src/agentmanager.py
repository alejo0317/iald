from flask import Flask
from flask import request
from flask.json import jsonify
from agent import Agent
import agent

class AgentManager:

    @staticmethod
    def search(keyword="", deep=3):
        a = Agent
        base_urls, repo_results = a.searchDatasetByKeyword(keyword)
        dataset_rdfs = {}
        # Download dataset RDFs
        data = {}
        agent.broken_count = 0
        agent.total_resources = 0
        agent.max_deep = deep
        #print "deep got: "+deep + " - max deep set: "+agent.max_deep
        resources_of_interest,sub_metrics = a.processRDFlist(base_urls, 1)
        metrics = {"repo_results": repo_results}
        metrics.update(sub_metrics)
        return resources_of_interest, metrics


app = Flask(__name__)
@app.route('/search', methods=['POST', 'GET'])
def search():
    #palabras = request.form['query']
    data = {}
    try:
        data['data'] = AgentManager.search()
        if data == {}:
            data = {'data': [{'nombre': "Cristian", 'descripcion': "Prueba", 'url': "URL", "tipo": "Persona"}], 'form': request.form}
    except Exception, e:
        data = {'data': [{'nombre': "Cristian", 'descripcion': "Prueba", 'url': "URL", "tipo": "Persona"}], 'form': request.form}
    return jsonify(**data)

# Obtengo un JSON con dos llaves 1) Data: RDFS validos, 2) Errors: Quer urls no se pudieron acceder y la razon (caidos, o pesan mucho). (data, errors), "URL ROTA", "TAMANO GRANDE"
