from ckanapi import RemoteCKAN
import rdflib
import urllib, os
from flask import jsonify
import json
import urllib2
REPO_URL = "http://datahub.io/"
API_URL = "https://datahub.io/api/3/action/"
BASE_DATASET_URL = "https://datahub.io/dataset/"

max_deep=1
broken_count = 0
total_resources = 0

def processMainRDF(url, deep):
    global broken_count
    global total_resources
    print "bc"+str(broken_count)
    print "tr"+str(total_resources)
    try:
        valid = Agent.validateUrlDownload(url)
        if valid:
            if deep > 1:
                rdf = Agent.parseRDF(url+".rdf")
            else:
                rdf = Agent.parseRDF(url)
            #rdf = Agent.parseRDF(url)
            sub_rdfs = Agent.queryRDF(rdf)
            #sub_rdfs = validateOthers(other_rdfs)
            total_resources += len(sub_rdfs)
        else:
            broken_count += 1
            sub_rdfs = []
    except Exception, e:
        broken_count += 1
        print "Excepcion 47, e: %s" % (str(e))
        sub_rdfs = []
    return sub_rdfs


def validateOthers(others):
    # por cada uno se valida si responde
    valid_others = []
    for o in others:
        if Agent.validateUrlDownload(o[1]):
            valid_others.append(o[1])
        else:
            broken_count += 1
    return valid_others

class Agent:

    def __init__(self):
        self.repo = RemoteCKAN(REPO_URL, get_only=True)

    @staticmethod
    def validateUrlDownload(url):
        try:
            print "=" * 100
            print "Valudando url {}".format(url)
            site = urllib2.urlopen(url, None, 5)
            meta = site.info()
            code = site.getcode()
            print "Respuesta: "+ str(code)
            if code == 404:
                return False
            else:
                return True
        except Exception, e:
            print e
            print "no valido"
            return False

    @staticmethod
    def parseRDF(url):
        print "Obteniendo RDF desde {}".format(url)
        g = rdflib.Graph()
        g.parse(url)
        return g

    # Search dataset  on repo (CkanObject)
    @staticmethod
    def searchDatasetByKeyword(keyword=""):
        print "="*100
        print "Buscando palabra {} y obteniendo el primer resultado".format(keyword)
        action = "package_search"
        if not keyword or keyword == "":
            site =  urllib.urlopen("https://datahub.io/api/3/action/package_search?fq=+tags:format-rdf+tags:education&rows=300")
        else:
            site =  urllib.urlopen("https://datahub.io/api/3/action/package_search?fq=+tags:format-rdf+tags:education&rows=300&q={}".format(keyword))
        respuesta = site.read()
        busqueda = json.loads(respuesta)
        print "Total de resultados: "+str(busqueda["result"]["count"])
        return [BASE_DATASET_URL + b['name'] + ".rdf" for b in busqueda['result']['results'] ],busqueda["result"]["count"]

    #Query over RDF graph
    @staticmethod
    def queryRDF(obj, query=None):
        print "Consultando RDFs"
        queries = ['SELECT ?description ?url WHERE { ?dist a dcat:Distribution. ?dist dct:description ?description. ?dist dcat:accessURL ?url. ?dist dcat:mediaType ?mediaType . FILTER regex (?mediaType, ".*rdf.*", "i") }', 'SELECT ?wp ?name WHERE { ?dist foaf:workplaceHomepage ?wp. ?dist foaf:name ?name. FILTER regex (?name, ".*Knowledge.*", "i") }']
        resultados = []
        for query in queries:
            try:
                res  = obj.query(query)
            except Exception, e:
                print "No se pudo ejecutar query, error: {}".format(str(e))
                res = []
            for r in res:
                resultados.append(r)
        print "Retornando consulta"
        return resultados

    @staticmethod
    def processRDFlist(rdf_list, deep):
        global broken_count
        global total_resources
        global max_deep
        print "max deep"+str(max_deep)
        print "=" * 100
        print "Start processing RDFs "+str(deep)
        multiple_results = []
        #sub_rdfs = []
        #import ipdb; ipdb.set_trace()
        #rdf_list.append("http://vivo.iu.edu/individual/IndianaUniversity/IndianaUniversity.rdf")
        print rdf_list
        for url in rdf_list:
            #result = {}
            #result['rdf-result'] = url
            sub_rdfs = processMainRDF(url,deep)
            #result['resources'] = sub_rdfs
            #print "deep: "
            #print type(deep)
            #print "max_deep: "
            #print type(max_deep)
            #print deep <= max_deep
            if deep < max_deep:
                print "=" * 100
                #deep+=1
                print "entrando a iteracion "+str(deep+1)
                sub_list = []
                for sub in sub_rdfs:
                    sub_list.append(sub[1])
                new_sub_rdfs, new_metrics = Agent.processRDFlist(sub_list, deep+1)
                #sub_rdfs.append(new_sub_rdfs)
                multiple_results+=new_sub_rdfs
                #result['sub_resources'] = new_sub_rdfs
            multiple_results+=sub_rdfs
        metrics = {'broken_count': broken_count, 'total_resources': total_resources}
        print "End processing RDFs"
        return multiple_results, metrics
