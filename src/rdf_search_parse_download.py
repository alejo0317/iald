from ckanapi import RemoteCKAN
import rdflib


BASE_DATASET_API = "https://datahub.io/dataset/"

# Function reads rdf from url or file
def parseRDF(url):
    print "Obteniendo RDF desde {}".format(url)
    g = rdflib.Graph()
    g.parse(url)
    return g

# Search dataset  on repo (CkanObject)
def searchDatasetByKeyword(repo, keyword="education"):
    print "Buscando palabra {} y obteniendo el primer resultado".format(keyword)
    busqueda = repo.call_action('package_search', {'q': keyword})
    b = busqueda['results'][0]
    print busqueda['results']
    name = b['name']
    return BASE_DATASET_API + name + ".rdf"

#Query over RDF graph
def queryRDF(obj, query):
    return obj.query(query)


###  BUSQUEDA EN REPOSITORIO

# Me conecto al repo
# Requiri'o de editar el modulo remoteckan y poner deshabilitar verify on request
repo = RemoteCKAN('http://datahub.io/', get_only=True)
## Busqueda educaci'on
print "Busqueda por palabra clave"
print searchDatasetByKeyword(repo, keyword="education")



### FILTRO DE RDFS
# RDF Filter for datahub resources
print "Ejemplo de consulta sobre sparql"
query = "SELECT ?url WHERE { ?dist a dcat:Distribution. ?dist dcat:accessURL ?url. ?dist dcat:mediaType 'meta/rdf-schema'. }"
# Descargo datos en RDF
f = parseRDF("https://datahub.io/dataset/world-bank-linked-data.rdf")
# Filtro solo los que contienen resources en RDF
res = queryRDF(f, query)
print "Filtro de recursos con formato RDF"
for i in res:
    print i



# Explorar el recurso y ver si tiene resultados relevantes, si los tiene tambien va como resultado de busqueda, sino se descarta. Si en un punto no se tiene el patron de busqueda seguir de ahi.

# Obtengo datasets de grupo
repo = RemoteCKAN('http://opendata.aragon.es/catalogo/')
datos_educacion  = repo.call_action('group_package_show', {'id':'educacion', 'limit':10})
b_edu = datos_educacion[0]
