from ckan_api_client.high_level import CkanHighlevelClient
from ckan_api_client.objects import CkanDataset

client = CkanHighlevelClient('http://datahub.io')
dataset_ids = client.list_datasets()


"""
import random
#select id
"""

dataset_id =  dataset_ids[100]

dataset = client.get_dataset(dataset_id)


org = client.get_organization_by_name('vocabularios-lod-bibliotecas')



ckan.logic.action.get.related_show(context, data_dict=None)¶

Parameters:
id (string) – id or name of the dataset (optional)
dataset (dictionary) – dataset dictionary of the dataset (optional)
type_filter (string) – the type of related item to show (optional, default: None, show all items)
sort (string) – the order to sort the related items in, possible values are ‘view_count_asc’, ‘view_count_desc’, ‘created_asc’ or ‘created_desc’ (optional)
featured (bool) – whether or not to restrict the results to only featured related items (optional, default: False)
Return type:
list of dictionaries


ckan.logic.action.get.related_list(context, data_dict=None)

Parameters:
id (string) – id or name of the dataset (optional)
dataset (dictionary) – dataset dictionary of the dataset (optional)
type_filter (string) – the type of related item to show (optional, default: None, show all items)
sort (string) – the order to sort the related items in, possible values are ‘view_count_asc’, ‘view_count_desc’, ‘created_asc’ or ‘created_desc’ (optional)
featured (bool) – whether or not to restrict the results to only featured related items (optional, default: False)
Return type:
list of dictionaries


ckan.logic.action.get.package_search(context, data_dict)
Searches for packages satisfying a given search criteria.

This action accepts solr search query parameters (details below), and returns a dictionary of results, including dictized datasets that match the search criteria, a search count and also facet information.

Solr Parameters:

For more in depth treatment of each paramter, please read the Solr Documentation.

This action accepts a subset of solr’s search query parameters:

Parameters:
q (string) – the solr query. Optional. Default: “*:*”
fq (string) – any filter queries to apply. Note: +site_id:{ckan_site_id} is added to this string prior to the query being executed.
sort (string) – sorting of the search results. Optional. Default: ‘relevance asc, metadata_modified desc’. As per the solr documentation, this is a comma-separated string of field names and sort-orderings.
rows (int) – the number of matching rows to return.
start (int) – the offset in the complete result for where the set of returned datasets should begin.
facet (string) – whether to enable faceted results. Default: “true”.
facet.mincount (int) – the minimum counts for facet fields should be included in the results.
facet.limit (int) – the maximum number of values the facet fields return. A negative value means unlimited. This can be set instance-wide with the search.facets.limit config option. Default is 50.
facet.field (list of strings) – the fields to facet upon. Default empty. If empty, then the returned facet information is empty.
The following advanced Solr parameters are supported as well. Note that some of these are only available on particular Solr versions. See Solr’s dismax and edismax documentation for further details on them:

qf, wt, bf, boost, tie, defType, mm



ckan.logic.action.get.resource_search(context, data_dict)
Searches for resources satisfying a given search criteria.

It returns a dictionary with 2 fields: count and results. The count field contains the total number of Resources found without the limit or query parameters having an effect. The results field is a list of dictized Resource objects.

The ‘query’ parameter is a required field. It is a string of the form {field}:{term} or a list of strings, each of the same form. Within each string, {field} is a field or extra field on the Resource domain object.

If {field} is "hash", then an attempt is made to match the {term} as a prefix of the Resource.hash field.

If {field} is an extra field, then an attempt is made to match against the extra fields stored against the Resource.

Note: The search is limited to search against extra fields declared in the config setting ckan.extra_resource_fields.

Note: Due to a Resource’s extra fields being stored as a json blob, the match is made against the json string representation. As such, false positives may occur:

If the search criteria is:

query = "field1:term1"
Then a json blob with the string representation of:

{"field1": "foo", "field2": "term1"}
will match the search criteria! This is a known short-coming of this approach.

All matches are made ignoring case; and apart from the "hash" field, a term matches if it is a substring of the field’s value.

Finally, when specifying more than one search criteria, the criteria are AND-ed together.

The order parameter is used to control the ordering of the results. Currently only ordering one field is available, and in ascending order only.

The fields parameter is deprecated as it is not compatible with calling this action with a GET request to the action API.

The context may contain a flag, search_query, which if True will make this action behave as if being used by the internal search api. ie - the results will not be dictized, and SearchErrors are thrown for bad search queries (rather than ValidationErrors).

Parameters:
query (string or list of strings of the form “{field}:{term1}”) – The search criteria. See above for description.
fields (dict of fields to search terms.) – Deprecated
order_by (string) – A field on the Resource model that orders the results.
offset (int) – Apply an offset to the query.
limit (int) – Apply a limit to the query.
Returns:
A dictionary with a count field, and a results field.
Return type:
dict




ckan.logic.action.get.tag_search(context, data_dict)
Return a list of tags whose names contain a given string.

By default only free tags (tags that don’t belong to any vocabulary) are searched. If the vocabulary_id argument is given then only tags belonging to that vocabulary will be searched instead.

Parameters:
query (string or list of strings) – the string(s) to search for
vocabulary_id (string) – the id or name of the tag vocabulary to search in (optional)
fields (dictionary) – deprecated
limit (int) – the maximum number of tags to return
offset (int) – when limit is given, the offset to start returning tags from
Returns:
A dictionary with the following keys:
'count'
The number of tags in the result.
'results'
The list of tags whose names contain the given string, a list of dictionaries.
Return type:
dictionarypip install pyutilib.component.corep



# Followers

ckan.logic.action.get.dataset_follower_count(context, data_dict)
Return the number of followers of a dataset.

Parameters:	id (string) – the id or name of the dataset
Return type:	int


ckan.logic.action.get.dataset_followee_count(context, data_dict)
Return the number of datasets that are followed by the given user.

Parameters:	id (string) – the id of the user
Return type:	int




ckan.logic.action.get.dataset_followee_list(context, data_dict)
Return the list of datasets that are followed by the given user.

Parameters:	id (string) – the id or name of the user
Return type:	list of dictionaries
import pdb; pdb.set_trace()
