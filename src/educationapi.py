from flask import Flask, jsonify, request, make_response

import agentmanager as a
import time

app = Flask(__name__)


@app.route('/education/api/v1.0/test', methods=['GET'])
def get_test():
    response = {"status": "ok", "method": "GET"}
    return jsonify(response)


@app.route('/education/api/v1.0/test', methods=['POST'])
def post_test():
    response = {"status": "ok", "method": "POST", "data": request.json}
    return jsonify(response)


@app.route('/education/api/', methods=['GET', 'POST'])
def search():
    init_time = time.time()
    try:
        keyword = request.args.get("keyword")
        deep = request.args.get("deep")
        #print "keyword="+keyword
        #print "deep="+deep
    except KeyError:
        keyword = ""
    deep=3
    data, metrics = a.AgentManager.search(keyword, int(deep))
    #results = rdf_search_parse_download.searchDatasetByKeyword()
    end_time = time.time()
    total_time = end_time - init_time
    all_metrics = {"total_time": total_time}
    all_metrics.update(metrics)
    response = {"metrics": all_metrics, "data": data}
    return jsonify(response)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(debug=True)
